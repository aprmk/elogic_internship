define([
    'jquery',
    'Magento_Ui/js/modal/modal',
    'JQueryUi',
    'domReady!'
], function($, modal) {
    'use strict';
    $.widget('elogic.giftr_popup', {
        options : {
            config:{
                'type': 'popup',
                'modalClass': 'popup_giftr',
                'responsive': true,
                'buttons': []
            },
            giftrModal: null
        },

        _create: function () {
            this._on('.giftr-link', {
                click: this.handler
            });
        },

        handler: function (event){
            event.preventDefault();

            if (!this.options.giftrModal){
                this.create(this.element);
            }

            this.show();
        },

        create: function (element){
            this.options.giftModal = element;
            modal(this.options.config, $(this.options.giftModal));
        },

        show: function () {
            $(this.options.giftModal).modal('openModal').trigger('contentUpdated');
        }
    })
    return $.elogic.giftr_popup;
});
