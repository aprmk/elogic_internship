define([
    "jquery",
    "swiper"
], function($, Swiper){

    $.widget('elogic.sliderswiper',{
        options: {
        },
        _create:function(config, element){
            console.log(this.element)
            var widget = this,
                options = this.getOptions(),
                swiper = new Swiper(this.element.context, options);

        },
        getOptions: function(){
            return this.options;
        },


    });
    return $.elogic.sliderswiper;
});
